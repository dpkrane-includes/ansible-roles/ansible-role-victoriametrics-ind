### [FEATURE]: Full functional molecule test added (HEAD -> main)
>Wed, 29 Mar 2023 14:16:08 +0300

>Author: Андрей Семисохин (semisohin@gmail.com)

>Commiter: Андрей Семисохин (semisohin@gmail.com)




### [FEATURE]: Full functional molecule test added (tag: v.0.0.2, origin/main)
>Wed, 29 Mar 2023 14:08:42 +0300

>Author: Андрей Семисохин (semisohin@gmail.com)

>Commiter: Андрей Семисохин (semisohin@gmail.com)




### Released (tag: v.0.0.1)
>Wed, 29 Mar 2023 11:49:07 +0300

>Author: Андрей Семисохин (semisohin@gmail.com)

>Commiter: Андрей Семисохин (semisohin@gmail.com)




### Initial
>Mon, 27 Mar 2023 11:15:50 +0300

>Author: Андрей Семисохин (semisohin@gmail.com)

>Commiter: Андрей Семисохин (semisohin@gmail.com)




